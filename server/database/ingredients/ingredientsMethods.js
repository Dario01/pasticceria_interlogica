const Ingredient=require('../model/ingredient');



exports.inserting=(req,res,next)=>{

    console.log(req.body)


    Ingredient.findOne({name:req.body.name}, function(err,existingIngredient){

            if(err){

                return next(err)
            }
            if(existingIngredient){
                return res.send({error:"This ingredient already exist"})
            }

            const ingredient=new Ingredient({
                name:req.body.name
            })
            ingredient.save()

            res.send(ingredient)

    })

}

exports.reading=(req,res,next)=>{

    Ingredient.find({}, function(err,result){


        if(err){
            return next(err)
        }

        res.send(result)

    })


}