const mongoose=require('mongoose')
const {Schema}=mongoose;



const SalesPriceListSchema=new Schema({
    cake:{
        type:Schema.Types.ObjectId,
        ref:'cake'
    },
    price:Number,
    choice:Number,
    pubblication_date:Date()
})


const SalesPriceList=mongoose.model('salesPriceList', SalesPriceListSchema);

module.exports=SalesPriceList