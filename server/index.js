const express=require('express');
const app=express()
const route=require('./route/index')
const bodyParser=require('body-parser')
const mongoose=require('mongoose')


mongoose.connect('mongodb://localhost:27017/pasticceria')


app.use(bodyParser.json())

route(app)

app.listen(4500)