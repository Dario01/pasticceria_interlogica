import React, {useEffect} from 'react';
import Login from './Login'
import Register from './Register';
import {BrowserRouter,Route, withRouter} from 'react-router-dom';
import RegisterSuccess from './RegisterSuccess';
import StartPage from './StartPage';
import Header from './Header';
import AdministratorPage from './AdministratorPage';
import {connect} from 'react-redux';
import * as actions from '../redux/actions/index';




function App(props){

    console.log('AUTH PROPS', props)

    useEffect(()=>{

        if(localStorage.getItem('token')){

            props.autoLogin(props.history)
        }
    }, [])


    function logout(){
        props.logout(props.history)
    }


    console.log('PROPS APP ',props)


    return(

        <div className="container">

           
            <Header logout={logout}/>
            <Route exact path="/" component={StartPage}/>
            <Route exact path="/login" component={Login}/>
            <Route exact path="/register" component={Register}/>
            <Route exact path="/success" component={RegisterSuccess}/>
            <Route exact path="/administratorPage" component={AdministratorPage}/>
            
            
        </div>
    )


}

function mapStateToProps(state){

    return{
        auth:state.auth.token
    }
}

export default connect(mapStateToProps,actions)(App);