import React, {useState} from 'react';
import {connect} from 'react-redux'
import * as actions from '../redux/actions/index'
import {withRouter} from 'react-router-dom';
import axios from 'axios'


function IngredientInsert(props){

   

    const [ingredientData, setIngredientData]=useState({name:'', result:''})

    function changeData(e){
        
        setIngredientData({name:e.target.value})
    }

    async function insertIngredient(){

            const res=await axios.post("/api/insert/ingredients",ingredientData);

            console.log(res)

            if(res.data.name){
                
                return setIngredientData({...ingredientData,result:'Insert OK'})
            }else{
                return setIngredientData({...ingredientData,result:'This ingredient already exists'})
            }

        }

    return(
        <div>
        <input type="text" value={ingredientData.name} onChange={changeData} />
        <button onClick={insertIngredient}>Insert Ingredient</button>
        <div>
        <h6>{ingredientData.result}</h6>
        </div>
        </div>

    )

}

export default withRouter(IngredientInsert);