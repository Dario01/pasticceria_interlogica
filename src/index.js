import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App'
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Provider} from 'react-redux';
import {BrowserRouter} from 'react-router-dom'
import store from './redux/store/store'



ReactDOM.render(
    <BrowserRouter>
<Provider store={store}>
    
<App/>

</Provider>
</BrowserRouter>
,document.getElementById('root'))