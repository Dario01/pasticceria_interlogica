import {SET_USER, SET_ERROR, SET_TOKEN} from './type'
import axios from 'axios';


export const autoLogin=(history)=>async dispatch=>{

    if(localStorage.getItem('token')){

       

        const headers={
            'Content-type':'application/json',
            'authorization':localStorage.getItem('token')
        }

       const res=await axios.get("/api/logged",{
           headers:headers
           
       })

        console.log('RES AUTO',res)

        if(res){
            console.log('INSIDE')
            
            dispatch({type:SET_TOKEN, payload:localStorage.getItem('token')})
        }else{
            localStorage.removeItem('token')
            history.push("/")
        }
        
    }else{
        history.push("/")
    }
}


export const register= (values, history)=>async (dispatch)=>{

    console.log('history',history)
    
    const res=await axios.post("/api/register", values)

    console.log('res',res)

    if(res.data.error){

        dispatch({type:SET_ERROR, payload:res.data.error})

    }else{

        history.push("/success")
    
    }
}


export const logout=()=>dispatch=>{

    localStorage.removeItem('token')
    dispatch({type:SET_TOKEN})
}


export const login=(values, history)=>async (dispatch)=>{

    console.log('FIRST',values)

    const res=await axios.post("/api/login", values)

    console.log('REEEES',res)

    if(res.data.error!==undefined){
        console.log('THIS ERROR')
        dispatch({type:SET_ERROR, payload:res.data.error})
    }else{
        console.log('rrre',res)
        console.log('NOT ERROR')
        localStorage.setItem('token',res.data.token)
        history.push("/administratorPage")
        dispatch({type:SET_TOKEN, payload:res.data.token})
        
    }


}